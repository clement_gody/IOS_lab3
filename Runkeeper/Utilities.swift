

import Foundation
import MapKit


extension MKMapView {
    func zoomToUserLocation() {
        guard let coordinate = userLocation.location?.coordinate else { return }
        let region = MKCoordinateRegionMakeWithDistance(coordinate, 200, 200)
        setRegion(region, animated: true)
    }
}

