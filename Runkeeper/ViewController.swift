
import UIKit
import MapKit
import CoreLocation
import HealthKit
import CoreData

class ViewController: UIViewController {
    
    
    @IBOutlet weak var stopbutton: UIButton!
    @IBOutlet weak var startbutton: UIButton!
    @IBOutlet weak var pacelabel: UILabel!
    @IBOutlet weak var distancelabel: UILabel!
    @IBOutlet weak var timelabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var seconds = 0.0
    var distance = 0.0
    
    lazy var locationManager: CLLocationManager = {
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest
        _locationManager.activityType = .fitness
        
        
        _locationManager.distanceFilter = 10.0
        return _locationManager
    }()
    
    lazy var locations = [CLLocation]()
    lazy var timer = Timer()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        startbutton.isHidden = false
        timelabel.isHidden = true
        distancelabel.isHidden = true
        pacelabel.isHidden = true
        stopbutton.isHidden = true
        mapView.isHidden = false
        
        locationManager.requestAlwaysAuthorization ()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
    }
    
    func eachSecond(timer: Timer) {
        seconds += 1
        let secondsQuantity = HKQuantity(unit: HKUnit.second(), doubleValue: seconds)
        timelabel.text = "Time: " + secondsQuantity.description
        let distanceQuantity = HKQuantity(unit: HKUnit.meter(), doubleValue: distance)
        distancelabel.text = "Distance: " + distanceQuantity.description
        
        let paceUnit = HKUnit.second().unitDivided(by: HKUnit.meter())
        let paceQuantity = HKQuantity(unit: paceUnit, doubleValue: seconds / distance)
        pacelabel.text = "Pace: " + paceQuantity.description
    }
    
    func startLocationUpdates() {
        
        locationManager.startUpdatingLocation()
    }

    @IBAction func startPressed(_ sender: Any) {
        
        startbutton.isHidden = true
        timelabel.isHidden = false
        distancelabel.isHidden = false
        pacelabel.isHidden = false
        stopbutton.isHidden = false
        mapView.isHidden =  false
        seconds = 0.0
        distance = 0.0
        locations.removeAll(keepingCapacity: false)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.eachSecond(timer:)), userInfo: nil, repeats: true)
    }

    @IBAction func StopPressed(_ sender: Any) {
        
        startbutton.isHidden = false
        
        
        timelabel.isHidden = true
        distancelabel.isHidden = true
        pacelabel.isHidden = true
        stopbutton.isHidden = true
        mapView.isHidden =  true
    }
}

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        mapView.showsUserLocation = (status == .authorizedAlways)
    }
    
    private func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        for location in locations as! [CLLocation] {
            let howRecent = location.timestamp.timeIntervalSinceNow
            
            if abs(howRecent) < 10 && location.horizontalAccuracy < 20 {
                
                if self.locations.count > 0 {
                    distance += location.distance(from: self.locations.last!)
                    
                    var coords = [CLLocationCoordinate2D]()
                    coords.append(self.locations.last!.coordinate)
                    coords.append(location.coordinate)
                    
                    let region = MKCoordinateRegionMakeWithDistance(location.coordinate, 500, 500)
                    mapView.setRegion(region, animated: true)
                    
                    mapView.add(MKPolyline(coordinates: &coords, count: coords.count))
                }
                
                
                self.locations.append(location)
            }
        }
    }
    
}

extension ViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let polyline = overlay as! MKPolyline
        let renderer = MKPolylineRenderer(polyline: polyline)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 3
        return renderer
        
    }
}

